# evant-progress-bar

> vue2.x 基于vue2.x的进度条progress-bar支持滑动拖拽，点击的进度条;

![img.png](https://gitee.com/evan_origin_admin/evant-progress-bar/raw/master/src/readme-resource/img1.png)
![img.png](	https://gitee.com/evan_origin_admin/evant-progress-bar/raw/master/src/readme-resource/img4.png)
![img.png](https://gitee.com/evan_origin_admin/evant-progress-bar/raw/master/src/readme-resource/img3.PNG)

**属性说明**
---
|  props   | 说明  | 类型  |默认值  |
|  ----  | ----  | ----  |----  |
| placement  | 进度数字显示的位置 | String类型; 可选值："top", "right"  | top |
| min  | 进度最小值 | Number  | 0  |
| max  | 进度最大值 | Number  | 100  |
| value  | v-model | Number  | -  |
| progressColor  | 进度条颜色 | String  | #409eff  |
| progressBgColor  | 进度条背景颜色 | String  | #e4e7ed  |
| thunkStyle  | 重写滑块圆圈的样式 | Object  | 例如：{'background': ''}  |
| blockStyleObject  | 重写滑块圆圈的内部block的样式 | Object  | 例如：{'background': ''}  |
---
**方法**
---
| 方法名称  | 说明  |  - | 
|  ----  | ----  | ----  |
|  getPercent  | 获取进度值  | getPercent(value)  |
---
**插槽**
---
| 插槽  | 位置  |  - | 
|  ----  | ----  | ----  |
|  icon  | 左侧图标  | - |

## 使用说明
```vue
<template>
  <evant-progress-bar
      v-model="percent"
      placement="right"
      progressColor="red"
      progressBgColor="yellow"
      :thunkStyleObject="{'background-color': 'blue', 'border-radius': '50%'}"
      :blockStyleObject="{'border': '1px solid red'}"
      @getPercent="getPercent"
  >
<!--    <template #icon>
      <i class="icon-notice"></i>
    </template>-->
  </evant-progress-bar>
</template>

<script>
import EvantProgressBar from "evant-progress-bar"
export default {
  components: { EvantProgressBar },
  data() {
    return {
      percent: 20
    }
  },
  methods: {
    getPercent(value) {
      console.log("当前进度值：", value)
    }
  }
}
</script>
```
